import 'package:flutter/material.dart';

class CartProvider with ChangeNotifier {
  int _cartCount;

  CartProvider() : _cartCount = 0;

  int get cartCount => _cartCount;

  void addItemToCart() {
    _cartCount++;
    notifyListeners();
  }
}
