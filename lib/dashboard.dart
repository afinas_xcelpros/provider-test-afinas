import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providertest/text_provider.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('======== RE-BUILDING======== ');
    TextProvider textProvider =
        Provider.of<TextProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Provider Test'),
      ),
      body: ListView(
//        shrinkWrap: true,
        padding: EdgeInsets.all(10),
        children: <Widget>[
          SizedBox(height: 100),
          Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: Colors.grey,
                    width: 2,
                  )),
//              child: Consumer<TextProvider>(
//                  builder: (BuildContext context, TextProvider value,
//                          Widget child) =>
                      child: Text(textProvider.text)),
//    ),
          SizedBox(height: 20),
          TextField(
            decoration: InputDecoration(
                hintText: 'Enter something',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10))),
            onChanged: (text) {
              textProvider.text = text;
            },
          ),
          SizedBox(height: 40),
          RaisedButton(
            color: Colors.blueAccent,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            padding: EdgeInsets.all(16),
            child: Text(
              "Go to Products",
              style: TextStyle(color: Colors.white),
            ),
            onPressed: () {
              Navigator.pushNamed(context, "products");
            },
          ),
        ],
      ),
    );
  }
}
