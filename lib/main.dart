import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providertest/cart_provider.dart';
import 'package:providertest/dashboard.dart';
import 'package:providertest/product_details_page.dart';
import 'package:providertest/product_list_page.dart';
import 'package:providertest/product_model.dart';
import 'package:providertest/text_provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => TextProvider()),
        ChangeNotifierProvider(create: (_) => CartProvider()),
        ChangeNotifierProvider(create: (_) => ProductsProvider()),
      ],
      child: MaterialApp(
        initialRoute: "/",
        debugShowCheckedModeBanner: false,
        routes: {
          "/": (_) => Dashboard(),
          "products": (_) => ProductListPage(),
          "details": (_) => ProductDetailsPage(),
        },
      ),
    );
  }
}
