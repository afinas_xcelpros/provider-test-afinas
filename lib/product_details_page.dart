import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providertest/cart_provider.dart';
import 'package:providertest/product_list_page.dart';
import 'package:providertest/product_model.dart';

class ProductDetailsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print('======== RE-BUILDING PRODUCT DETAILS ======== ');
    CartProvider cartProvider =
        Provider.of<CartProvider>(context, listen: false);
    final ProductModel model = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: MyAppBar(title: model.productName),
      body: ListView(
        padding: EdgeInsets.all(16),
        children: <Widget>[
          Hero(
              tag: "${model.productName}image",
              child: Placeholder(fallbackHeight: 200)),
          SizedBox(height: 20),
          Hero(
            child: Text(model.description),
            tag: "${model.description}",
          ),
          SizedBox(height: 20),
          FlatButton.icon(
              padding: EdgeInsets.all(10),
              onPressed: () {
                cartProvider.addItemToCart();
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16)),
              color: Colors.blueAccent,
              icon: Icon(Icons.add_shopping_cart, color: Colors.white),
              label: Text(
                "Add to cart",
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
    );
  }
}
