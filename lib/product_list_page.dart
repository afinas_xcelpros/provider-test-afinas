import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providertest/cart_provider.dart';
import 'package:providertest/product_model.dart';

class ProductListPage extends StatefulWidget {
  @override
  _ProductListPageState createState() => _ProductListPageState();
}

class _ProductListPageState extends State<ProductListPage> {

  CartProvider cartProvider;
  ProductsProvider productsProvider;
//  StreamController _streamController;
  final List<ProductModel> list = [];

  @override
  void initState() {
    cartProvider = Provider.of<CartProvider>(context, listen: false);
    productsProvider = Provider.of<ProductsProvider>(context, listen: false);
//    _streamController = StreamController<ProductModel>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('======== RE-BUILDING PRODUCT LISTING ======== ');
    return Scaffold(
      appBar: MyAppBar(title: "Product List"),
      body: StreamBuilder<ProductModel>(
          stream: productsProvider.getStream(),
          builder: (context, snapshot) {
            if(snapshot.data != null)
            list.add(snapshot.data);
            return ListView.separated(
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) {
                ProductModel data = list[index];
                return ListTile(
                  leading: SizedBox(
                      height: 40,
                      width: 40,
                      child: Hero(
                        tag: "${data.productName}image",
                        child: Placeholder(),
                      )),
                  subtitle: Text(data.description),
                  title: Text(data.productName),
                  trailing: FlatButton.icon(
                      onPressed: () {
                        cartProvider.addItemToCart();
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16)),
                      color: Colors.blueAccent,
                      icon: Icon(Icons.add_shopping_cart, color: Colors.white),
                      label: Text(
                        "Add to cart",
                        style: TextStyle(color: Colors.white),
                      )),
                  onTap: () {
                    Navigator.pushNamed(context, "details", arguments: data);
                  },
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  Divider(
                    height: 2,
                    color: Colors.grey,
                  ),
            );
          }),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.plus_one),
        onPressed: () {
          productsProvider.addProduct();
        },
      ),
    );
  }


}

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;

  MyAppBar({this.title});

  @override
  Widget build(BuildContext context) {
    print('======== RE-BUILDING APP BAR ======== ');
    return AppBar(
      title: Text(title ?? ""),
      actions: <Widget>[
        CartIcon(iconData: Icons.shopping_cart),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

class CartIcon extends StatelessWidget {
  final IconData iconData;
  final VoidCallback onTap;

  const CartIcon({
    Key key,
    this.onTap,
    @required this.iconData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('======== RE-BUILDING TOP CART ICO ======== ');
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Icon(
              iconData,
              size: 35,
            ),
            Positioned(
              top: 5,
              right: 0,
              child: Container(
                constraints: BoxConstraints(minWidth: 25),
                padding: EdgeInsets.all(4),
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(30)),
                alignment: Alignment.center,
                child: Consumer<CartProvider>(
                    builder: (BuildContext context, CartProvider value,
                        Widget child) =>
                        Text('${value.cartCount ?? 0}')),
              ),
            )
          ],
        ),
      ),
    );
  }
}
