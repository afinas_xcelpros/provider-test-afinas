import 'dart:async';

import 'package:flutter/material.dart';

class ProductsProvider with ChangeNotifier {
  StreamController<ProductModel> _streamController;
  int _i = 0;

  ProductsProvider() {
    _streamController = StreamController<ProductModel>.broadcast();
    productList.forEach((element) {
      _streamController.add(element);
    });

    Timer.periodic(Duration(seconds: 2), (timer) {
      addProduct();
    });
  }

  Stream<ProductModel> getStream() => _streamController.stream;

  addProduct() {
    _i++;
    _addProduct(ProductModel(
        productName: "ProductName $_i",
        description: "Description $_i",
        price: "$_i"));
  }

  _addProduct(ProductModel model) {
    _streamController.add(model);
  }
}

class ProductModel {
  String productName;
  String description;
  String price;

  ProductModel({
    this.productName,
    this.description,
    this.price,
  });
}

List<ProductModel> productList = [
  ProductModel(
    productName: "Product 1",
    description: "Description 1",
    price: "Rs.111",
  ),
  ProductModel(
    productName: "Product 2",
    description: "Description 2",
    price: "Rs.222",
  ),
  ProductModel(
    productName: "Product 3",
    description: "Description 3",
    price: "Rs.333",
  ),
  ProductModel(
    productName: "Product 4",
    description: "Description 4",
    price: "Rs.444",
  ),
];
