import 'package:flutter/material.dart';

class TextProvider with ChangeNotifier {
  String _text;

  TextProvider() : _text = "";

  set text(String value) {
    _text = value;
    notifyListeners();
  }

  String get text => _text;
}
